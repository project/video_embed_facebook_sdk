(function($) {

Drupal.videoEmbedFacebookSdk = Drupal.videoEmbedFacebookSdk || {};

/**
 * Saves the playing video.
 */
Drupal.videoEmbedFacebookSdk.inPlay = null;

/**
 * Saves all players.
 */
Drupal.videoEmbedFacebookSdk.players = {};

/**
 * Deletes the referencing of the player.
 */
Drupal.videoEmbedFacebookSdk.clearPlay = function(e) {
  Drupal.videoEmbedFacebookSdk.inPlay = null;
};

/**
 * Function that the SDK calls after loading.
 */
var videoFbAsyncInit = function() {
  var $vm = Drupal.videoEmbedFacebookSdk;
  FB.init({
    // appId: 'no need it'
    xfbml      : true,
    version    : 'v2.5'
  });

  FB.Event.subscribe('xfbml.ready', function(msg) {
    if (msg.type === 'video') {
      var domVideo = $('#' + msg.id);
      domVideo.once('video-embed-facebook-sdk', function() {
        var dataVideo = domVideo.data();
        var player = msg.instance;
        $vm.players[msg.id] = player;
        player.subscribe('startedPlaying', function(e) {
          // Stops the current video.
          if ($vm.inPlay) {
            $vm.inPlay.pause();
          }
          $vm.inPlay = player;
        });
        player.subscribe('paused', $vm.clearPlay);
        player.subscribe('finishedPlaying', $vm.clearPlay);
        if (dataVideo.vefsdk.autoplay) {
          player.play();
        }
      });
    }
  });
}

/**
 * Be sure to call videoFbAsyncInit after ajax.
 */
Drupal.behaviors.videoEmbedFacebookSdk = {
  attach: function(context, settings) {
    if (typeof FB != 'undefined') {
      videoFbAsyncInit();
    }
  }
}

/**
 * If another window.fbAsyncInit exists.
 */
if (typeof window.fbAsyncInit == 'function') {
  var originFbAsyncInitFunction = window.fbAsyncInit;
  window.fbAsyncInit = function() {
    originFbAsyncInitFunction();
    videoFbAsyncInit();
  }
}
else {
  window.fbAsyncInit = videoFbAsyncInit;
}

})(jQuery);
