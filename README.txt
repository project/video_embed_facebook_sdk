# Video Embed Facebook SDK

Extends the handler for Facebook videos to use SDK.js in Video Embed Field. This use the api from here.

## Dependencies
* Video Embed Facebook (From Video Embed Field)

## Features
* Use the Facebook player.
* Can autoplay.
* Show text from facebook post.
* Show caption.
